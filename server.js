//This adds the project root to the NODE_PATH when the script is loaded. This allows me to require any file in my project by referencing its relative path from the project root such as var User = require('models/user')
process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();

//Create server via express
var express = require('express'), 
app = express(),
port = process.env.PORT || 300; //Assign port 300

//Register routes file
var routes = require('./api/routes/routes.js');
routes(app);

//Add middleware to perform authentication/validations
app.use(function(req, res){

    //If url is invalid
    res.status(404).send({error: req.originalURL + ' not found'});
});

app.listen(port);
console.log('TravelBigData API server started');