'use strict';

//Library for common functions

//Include Libraries
var request = require('sync-request');


//Funcion to get json contents from url
exports.GetJSONContents = function(url, headers=null) {

    //Get response from URL via sync request
    let response = request('GET', url, headers);

    //Parse response in JSON format and return it
    return JSON.parse(response.getBody());
}


//Function to get url contents from url
exports.GetURLContents = function(url){

    //Get response from URL via sync request
    let response = request('GET', url);

    return response.getBody();
}