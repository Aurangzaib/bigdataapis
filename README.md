# social-media-api
NodeJs Web API to fetch images, videos and posts from all social media websites.

List of social media.

Images
 - Twitter
 - Instagram
 - Flickr
 - Pinterest
 - Tumblr
 - Weibo
 - WeHeartIt

Videos
 - Twitter
 - Youtube
 - Metacafe
 - Flickr
 
Posts
- Twitter
- Reddit
