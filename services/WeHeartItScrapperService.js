'Use Strict';

//Include libraries
var Globals = require('lib/globals.js');
var Common = require('lib/common');
var cheerio = require('cheerio');
//var fs = require('fs');

//Function to scrap WeHeartIt images against keyword
exports.ScrapImages = function(keyword, page_no) {

    //Declare result array
    let result_arr = {'Images': []}, url, html, $;
    let image_url, username, user_prof_pic, user_prof_url, count_likes;

    //Prepare url
    url = Globals.WeHeartIt_API + '?query=' + keyword + '&scrolling=true&page=' + page_no;

    //Call common lib function to get url contents
    html = Common.GetURLContents(url);
    //fs.writeFile('weheartit.html', html, function(err){} );

    //Parse html using cheerio
    $ = cheerio.load(html);

    //Iterate through html elements
    $('.entry.grid-item').each( (i, element) =>{
        
        //Extract metadata
        image_url = $(element).find('img.entry-thumbnail').attr('src');
        username = $(element).find('span.text-big').text();
        user_prof_pic = $(element).find('.avatar-container img.avatar').attr('src');
        user_prof_url = Globals.WeHeartIt + $(element).attr('data-uploader-username');
        count_likes = $(element).find('.js-heart-count').text();

        //Add in result array
        result_arr['Images'].push({'image_url': image_url, 'username': username, 'user_prof_pic': user_prof_pic, 'user_prof_url': user_prof_url, 'count_likes': count_likes });
    });

    //Return result array
    return result_arr;    
}