'use strict';

//Include libraries
var Globals = require('../lib/globals.js') ;
var Common = require('lib/common');


//Function to scrap instagam images
exports.ScrapImages = function(keyword, end_cursor) {


    //Declare result array
    let result_arr = {'Images': [], 'end_cursor': '' }, url, hashtag, res, json_data, hashtags_path;
    let image_url, post_url, likes_count, comments_count, shortcode;

    //Check if end cursor is null (first request)
    if (end_cursor == null)
    {
        //Prepare url to get first hashtag against given keyword
        url = Globals.Instagram_HashtagsAPI + '?query=' + keyword;

        //Call common lib function to get json response
        json_data = Common.GetJSONContents(url);

        //Get first hashtag from response
        hashtag = json_data.hashtags[0].hashtag.name;

        //Prepare url to get images against hashtag
        url = Globals.Instagram_API + hashtag + '/?__a=1'; 
    }  
    else
    {
        //Prepare url to get images from given end cursor
        url = Globals.Instagram_API + keyword + '/?__a=1&end_cursor=' + end_cursor;
    }    

    //Call common lib function to get json response
    json_data = Common.GetJSONContents(url);

    //Get end_cursor for next call
    hashtags_path = json_data.graphql.hashtag;
    result_arr['end_cursor'] = hashtags_path.edge_hashtag_to_media.page_info.end_cursor;

    //Iterate through top images array 
    hashtags_path.edge_hashtag_to_top_posts.edges.forEach(element =>{

        //Extract metadata
        image_url = element.node.display_url;
        post_url = Globals.Instagram + 'p/' + element.node.shortcode;
        likes_count = element.node.edge_liked_by.count;
        comments_count = element.node.edge_media_to_comment.count;
        shortcode = element.node.shortcode;

        //Add metadata in result array
        result_arr['Images'].push({'image_url': image_url, 'post_url': post_url, 'likes_count': likes_count, 'comments_count': comments_count, 'shortcode': shortcode} );

        //url to get info about post owner's profile
        //https://www.instagram.com/p/B8OjIKAgwRx/?__a=1
    });

    //Iterate through images array
    hashtags_path.edge_hashtag_to_media.edges.forEach(element =>{

        //Extract metadata
        image_url = element.node.display_url;
        post_url = Globals.Instagram + 'p/' + element.node.shortcode;
        likes_count = element.node.edge_liked_by.count;
        comments_count = element.node.edge_media_to_comment.count;
        shortcode = element.node.shortcode;

        //Add metadata in result array
        result_arr['Images'].push({'image_url': image_url, 'post_url': post_url, 'likes_count': likes_count, 'comments_count': comments_count, 'shortcode': shortcode });
    });

    //Return result array
    return result_arr;
}

