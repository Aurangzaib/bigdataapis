'use strict';

//Include libraries
var Globals = require('lib/globals');
var Common = require('lib/common');
var cheerio = require('cheerio');

//Function to scrap Youtube videos
exports.ScrapVideos = function(keyword, page_index_code){

    //Declare result array
    let result_arr = {'Videos':[], 'Page_Indexes':[] }, url, html, $;
    let video_url, thumbnail_url, title, description, channel_name, channel_link, views_count, page_index, i;

    //Prepare url
    url = Globals.Youtube_API + keyword;

    //Check if page index code is provided (not first request)
    if (page_index_code != null)
        url = url + '&sp=' + page_index_code;

    //Call Common lib function to get URL contents
    html = Common.GetURLContents(url);

    //Load html via cheerio lib (for parsing)
    $ = cheerio.load(html);

    //Iterate through video divs
    $('ol.item-section li div.yt-lockup').each( (i, element) => {

        //Extract metadata
        video_url = Globals.Youtube + $(element).find('a.spf-link').attr('href');

        //Check if img has data-thumb attribute
        if ($(element).find('span.yt-thumb-simple img').attr('data-thumb') )
            thumbnail_url = $(element).find('span.yt-thumb-simple img').attr('data-thumb'); //Extract data-thumb attribute
        else
            thumbnail_url = $(element).find('span.yt-thumb-simple img').attr('src'); //Else extract src attribute

        title = $(element).find('.yt-lockup-title a').attr('title');
        description = $(element).find('.yt-lockup-description').text().trim();
        channel_name = $(element).find('.yt-lockup-byline a').text().trim();
        channel_link = Globals.Youtube + $(element).find('.yt-lockup-byline a').attr('href');
        views_count = $(element).find('.yt-lockup-content .yt-lockup-meta-info li:nth-child(2)').text();
        views_count = views_count.split(' ')[0];

        //Add data in result array
        result_arr['Videos'].push({'video_url': video_url, 'thumbnail_url': thumbnail_url, 'title': title, 'description': description, 'channel_name': channel_name, 'channel_link': channel_link, 'views_count': views_count});
    });

    //Iterate through page links 
    for (i=1; i<$('.search-pager .yt-uix-button').length; i++)
    {
        //Check attribute (data-redirect-url) exists
        if ($('.search-pager .yt-uix-button:nth-child(' +i+ ')').attr('data-redirect-url'))
            page_index = $('.search-pager .yt-uix-button:nth-child(' +i+ ')').attr('data-redirect-url').split('sp=')[1];
        else
            page_index = $('.search-pager .yt-uix-button:nth-child(' +i+ ')').attr('href').split('sp=')[1];

        result_arr['Page_Indexes'].push({'page_no': i, 'page_index': page_index });
    }

    //Return result array
    return result_arr;
}