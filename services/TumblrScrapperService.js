'use strict';

//Include libraries
var Globals = require('lib/globals');
var Common = require('lib/common');
var cheerio = require('cheerio');
var puppeteer = require('puppeteer'); //For scroll scrapping


//Function to scrap Tumblr Images
exports.ScrapImages = function(keyword){

    //Declare result array
    let result_arr = {'Images': []}, url, html, $, header, post;
    let username, user_prof_link, user_prof_pic, post_contents='';

    //Prepare url
    url = Globals.Tumblr + keyword;

    //Call common lib function to get URL contents
    html = Common.GetURLContents(url);

    //Parse html via cheerio 
    $ = cheerio.load(html);

    //Iterate through html 
    $('#search_posts article').each( (i, element) => {

        let image_url = '', post_body='', notes_count='', post_tags_arr='', post_tags='';

        //Get html parent eleemnts
        header = $(element).find('header .tumblelog_info');
        post = $(element).find('.post_content');
        post_tags_arr = $(element).find('.post_tags .post_tag');

        //Extract metadata
        username = $(header).find('a').attr('title');
        user_prof_pic = $(header).find('a').attr('data-tumblelog-popover').split('"')[3];
        user_prof_link = $(header).find('a').attr('href');

        //Check if image exists under post_media 
        if ($(post).find('.post_media img').length)
            image_url = $(post).find('.post_media img').attr('src');
        else if ($(post).find('.post_content_inner .post_body img').length) //else under post_body
            image_url = $(post).find('.post_content_inner .post_body img').attr('src');

        post_contents = $(post).find('.post_content_inner .post_body').text().trim(); //.html() to get innertext including html
        notes_count = $(element).find('.post_notes .note_link_current').text().split(' ')[0];

        //Check if image does not exist
        if (image_url == '')
            post_body = post_contents;

        //Iterate through post tags array 
        $(post_tags_arr).each((i, element) => {

            post_tags = post_tags + ' #' + $(element).attr('data-tag');
        });
            

        //Add data in result array
        result_arr['Images'].push({'username': username, 'user_prof_pic': user_prof_pic, 'user_prof_link': user_prof_link, 'image_url': image_url, 'post_body': post_body, 'post_contents': post_contents, 
                                   'post_tags': post_tags.trim(), 'notes_count': notes_count });
    });

    //Return result array
    return result_arr; 
}


//Function to scrap Tumblr images with scroll
exports.ScrollImages = function(keyword, res)
{
     //Declare result array
     let result_arr = {'Images':[]}, url;

    //Prepare url
    url = Globals.Tumblr + keyword;

     (async () => {

        //Set up browser and url
        const browser = await puppeteer.launch({
            headless: false,
            args: ['--no-sandbox', '--disable-setuid-sandbox'],
        });
        const page = await browser.newPage();

        // Navigate to url
        await page.goto(url);

        // Scroll and scrap data from url
        let items_arr = await ScrapeInfiniteScrollItems(page, ScrapData, 30);

        //Add data in result array
        result_arr['Images'].push(items_arr);
    
        // Close the browser.
        await browser.close();

        //Return response in JSON format
        res.json(result_arr);
    })();
    
}



//Function to scrap data
function ScrapData() 
{
    //Declare result array
    let items_arr = [], header, post, post_tags_arr, username, user_prof_link, user_prof_pic, post_contents, notes_count;

    //Get article element
    let article = document.querySelectorAll('#search_posts article');
    
    //Iterate through aricle elements
    for (let node of article) {

        let image_url = '', post_body='', post_tags='';

        //Extract metadata
        header = node.querySelector('header .tumblelog_info a');
        post = node.querySelector('.post_content');
        post_tags_arr = node.querySelectorAll('.post_tags .post_tag');
        
        username = header.getAttribute('title');
        user_prof_pic = header.getAttribute('data-tumblelog-popover').split('"')[3];
        user_prof_link = header.getAttribute('href');
        
        //Check if post body exists
        if (post.querySelector('.post_body'))
            post_contents = post.querySelector('.post_body').textContent.trim();

        //Check if image exists
        if (post.querySelector('img'))
            image_url = post.querySelector('img').getAttribute('src');

        //Check if is empty
        if (image_url == '')
            post_body = post_contents;

        //Notes count
        notes_count = node.querySelector('.post_footer .note_link_current').innerText.split(' ')[0];

        //Post tags
        post_tags_arr.forEach(element => {
            post_tags = post_tags + ' #' + element.getAttribute('data-tag');
        });

        //Add data in items array
        items_arr.push( {'username': username, 'user_prof_pic': user_prof_pic, 'user_prof_link': user_prof_link, 'image_url': image_url, 'post_contents': post_contents, 'post_body': post_body, 'post_tags': post_tags.trim(), 'notes_count': notes_count } );
    }

    //Return items array
    return items_arr;
}



async function ScrapeInfiniteScrollItems(page, extractItems, itemTargetCount, scrollDelay = 1000,) 
{
  let items = [];
  try {
    let previousHeight;
    while (items.length < itemTargetCount) {
      items = await page.evaluate(extractItems);
      previousHeight = await page.evaluate('document.body.scrollHeight');
      await page.evaluate('window.scrollTo(0, document.body.scrollHeight)');
      await page.waitForFunction(`document.body.scrollHeight > ${previousHeight}`);
      await page.waitFor(scrollDelay);
    }
  } catch(e) { }
  return items;
}
