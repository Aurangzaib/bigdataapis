'use strict';

//Include libraries
var Globals = require('lib/globals');
var Common = require('lib/common');


//Function to scrap pinterest images
exports.ScrapImages = function(keyword, bookmark){

    //Declare result array
    let result_arr = {'Images': [], 'bookmark': null}, url, json_data;
    let description, post_url, userdisplayname, user_prof_pic, user_prof_url, ref_site, ref_site_url;

    //Prepare url
    url = Globals.Pinterest_API.replace(/keyword/g, keyword);

    //Check if bookmark is not null (not first call)
    if (bookmark != null)
        url = url.replace('bmark', bookmark);
    else
        url = url.replace('bmark', '');

    //Call common lib function to get JSON contents
    json_data = Common.GetJSONContents(url);

    //Add bookmark (from response) in result array for next call
    result_arr['bookmark'] = json_data['resource']['options']['bookmarks'][0];

    //Iterate through pins array
    json_data['resource_response']['data']['results'].forEach(node => {

        //Extract data
        url = node['images']['orig']['url'];
        description = node['grid_title'];
        post_url = Globals.Pinterest + 'pin/' + node['id'];
        userdisplayname = node['pinner']['full_name'];
        user_prof_pic = node['pinner']['image_medium_url'];
        user_prof_url = Globals.Pinterest + node['pinner']['username'];
        ref_site = node['domain'];
        ref_site_url = node['link'];

        //Add data in result array
        result_arr['Images'].push({'url': url, 'description': description, 'post_url': post_url, 'userdisplayname': userdisplayname, 'user_prof_pic': user_prof_pic, 'user_prof_url': user_prof_url, 'ref_site': ref_site, 'ref_site_url': ref_site_url });
    });
    
    //Return result array
    return result_arr;
}