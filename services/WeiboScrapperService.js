'use strict';

//Include libraries
var Globals = require('lib/globals.js');
var Common = require('lib/common');


//Function to scrap Weibo images against keyword
exports.ScrapImages = function(keyword, page_no){

    //Declare result array
    let result_arr = {'Images': []}, url, headers, json_data;
    let description, sub_description, username, user_prof_pic, user_prof_url;

    //Prepare url
    url = Globals.Weibo_API + '?q='+ keyword + '&page=' + page_no;

    //Call common lib function to get JSON contents against url
    json_data = Common.GetJSONContents(url, GetReqHeaders(keyword)); //Gat request headers from function

    //Iterate through items
    json_data['data']['pic_list'].forEach(node => {

        //Extract metadata
        url = node['url'];
        description = node['text'];
        sub_description = node['sub_text'];
        username = node['user']['name'];
        user_prof_pic = node['user']['profile_image_url'];
        user_prof_url = node['user']['profile_url'];

        //Add in result array
        result_arr['Images'].push({'url': url, 'description': description, 'sub_description': sub_description, 'username': username, 'user_prof_pic': user_prof_pic, 'user_prof_url': user_prof_url }); 

    });

    //Return result array
    return result_arr;
}


//Function to scrap videos
exports.ScrapVideos = function(keyword){

    
}


//Function to get request headers
let GetReqHeaders = function(keyword){

    let referer = Globals.Weibo + '?q=' + keyword + '&Refer=Spic_box';
    return { headers: {'X-Requested-With': 'XMLHttpRequest', 'Referer': referer} };
}