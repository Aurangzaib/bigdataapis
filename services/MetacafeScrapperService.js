'use strict';

//Include libraries
var Globals = require('lib/globals');
var Common = require('lib/common');
var cheerio = require('cheerio');

//Function to scrap Metacafe videos
exports.ScrapVideos = function(keyword){

    //Declare result array
    let result_arr = {'Videos': []}, url, $, html;
    let video_url, thumbnail_url, title, description, channel_name, channel_link, views_count;

    //Replace space with _ in keyword (as per metacafe search link)
    keyword = keyword.replace(' ', '_') + '_';

    //Prepare url
    url = Globals.Metacafe_API.replace('keyword', keyword);

    //Call Common lib function to get URL contents
    html = Common.GetURLContents(url);

    //Load html via cheerio lib (for parsing)
    $ = cheerio.load(html);

    //Iterate through video divs
    $('.mc-previews .mc-preview').each( (i,element) => {

        //Extract metadata
        video_url = $(element).find('.mc-preview-image a').attr('href');
        thumbnail_url = $(element).find('.mc-preview-image img').attr('src');


        //Add in result array
        result_arr['Videos'].push({'video_url': video_url, 'thumbnail_url': thumbnail_url, 'title': title, 'description': description, 'channel_name': channel_name, 'channel_link': channel_link, 'views_count': views_count});
    });

    //Return result array
    return result_arr;

}