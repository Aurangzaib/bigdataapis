'use strict';

//Include libraries
var Globals = require('lib/globals.js');
var Common = require('lib/common');

//Function to scrap Flickr images
exports.ScrapImages = function(keyword, page_no, api_key){

    //Declare result array
    let result_arr = {'Images': [], 'api_key': null}, url, json_data;
    let username, user_prof_pic, user_prof_url, post_url, pic_title, faves_count, comments_count;

    //Check if api_key is null (first call)
    if (api_key == null)
    {
        //Url to extract api_key
        url = Globals.Flickr + 'search/?text=travel&media=photos';

        //Call function to extract API key (site_key)
        api_key = Extract_APIKey(url);
    }

    //Add api_key in result array for next call
    result_arr['api_key'] = api_key;

    //Prepare url 
    url = Globals.Flickr_API + '&page=' + page_no + '&api_key=' + api_key + '&text=' + keyword;

    //Call commn lib function to get json contents
    json_data = Common.GetJSONContents(url);

    //Iterate through photos array
    json_data['photos']['photo'].forEach(photo_node => {

        //Extract info
        url = photo_node['url_w'];
        username = photo_node['realname'];
        user_prof_pic = Globals.Flickr_Photos_URL + photo_node['server'] + '/buddyicons/' + photo_node['owner'] + '_l.jpg';
        user_prof_url = Globals.Flickr + 'photos/' + photo_node['pathalias'];
        pic_title = photo_node['title'];
        post_url = Globals.Flickr + 'photos/' + photo_node['pathalias'] + '/' + photo_node['id'];
        faves_count = photo_node['count_faves'];
        comments_count = photo_node['count_comments'];

        //Add in result array
        result_arr['Images'].push({'url': url, 'username': username, 'user_prof_pic': user_prof_pic, 'user_prof_url': user_prof_url, 'pic_title': pic_title, 'post_url': post_url, 'faves_count': faves_count, 'comments_count': comments_count } );
    });

    //Return result array
    return result_arr;

}


//Function to extract API key (site_key) from Flickr url
let Extract_APIKey = function(url){

     //Declare variables
     let html, key_pos, html_str;

    //Call common lib function to get html contents
    html = Common.GetURLContents(url);

    //Convert html to string
    html_str = html.toString();

    //Get site_key variable position
    key_pos = html_str.indexOf('site_key =') + 10;

    //Extract site_key value and return
    return html_str.substring(key_pos).split('"')[1];
}
