'use strict';

//Include libraries
var Globals = require('lib/globals');
var Common = require('lib/common');
var puppeteer = require('puppeteer');
var fs = require('fs');

//Function to scrap TikTok images
exports.ScrapImages = function(keyword){

    //Declare result array
    let result_arr = {'Images': []}, url;

    //Prepare url
    url = 'https://www.tiktok.com/trending/?lang=en';

     (async () => {

        //Set up browser and url
        const browser = await puppeteer.launch({
            headless: false,
            args: ['--no-sandbox', '--disable-setuid-sandbox'],
        });
        const page = await browser.newPage();

        // Navigate to url
        await page.goto(url);

        // Scroll and scrap data from url
        let items_arr = await ScrapeInfiniteScrollItems(page, ScrapData, 100);

        //Add data in result array
        console.log('blah');
    
        // Close the browser.
        //await browser.close();

        //Return response in JSON format
        //res.json(result_arr);
    })();
}


//Function to scrap data
function ScrapData() 
{
    //Declare result array
    let items_arr = [];

    //Get article element
    //let base_element = document.querySelectorAll('.share-layout-main .video-feed');
    
    //Iterate through aricle elements
    //for (let node of base_element) {
       
    //}

    //Return items array
    return items_arr;
}



async function ScrapeInfiniteScrollItems(page, extractItems, itemTargetCount, scrollDelay = 1000,) 
{
  let items = [];
  try {
    let previousHeight;
    while (items.length < itemTargetCount) {
      items = await page.evaluate(extractItems);
      previousHeight = await page.evaluate('document.body.scrollHeight');
      await page.evaluate('window.scrollTo(0, document.body.scrollHeight)');
      await page.waitForFunction(`document.body.scrollHeight > ${previousHeight}`);
      await page.waitFor(scrollDelay);
    }
  } catch(e) { }
  return items;
}
