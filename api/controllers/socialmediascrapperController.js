'use strict';

//Including social media scrapper services
var TwitterScrapperService = require('services/TwitterScrapperService');
var InstagramScrapperService = require('../../services/InstagramScrapperService');
var FlickrScrapperService = require('services/FlickrScrapperService');
var PinterestScrapperService = require('services/PinterestScrapperService');
var TumblrScrapperService = require('services/TumblrScrapperService');
var TikTokScrapperService = require('services/TikTokScrapperService');
var WeiboScrapperService = require('services/WeiboScrapperService');
var WeHeartItScrapperService = require('services/WeHeartItScrapperService');
var YoutubeScrapperService = require('services/YoutubeScrapperService');
var MetacafeScrapperService = require('services/MetacafeScrapperService');

//Function to get Twitter images
exports.TwitterImages = function(req, res){
    //res.json({message: 'Output goes gere'});
    //res.send('string response');

    //Call service function to scrap Twitter images
    res.json(TwitterScrapperService.ScrapImages(req.params.keyword));
}

//Function to get Instagram images
exports.InstagramImages = function(req, res){

    //Call service function to scrap Instagram images
    res.json(InstagramScrapperService.ScrapImages(req.params.keyword, req.params.end_cursor));
}

//Function to get Pinterest images
exports.PinterestImages =  function(req, res){

    //Call service function to scrap Pinterest images
    res.json(PinterestScrapperService.ScrapImages(req.params.keyword, req.params.bookmark));
}

//Function to get Flickr images
exports.FlickrImages = function(req, res){

    //Call service function to scrap Flickr images
    res.json(FlickrScrapperService.ScrapImages(req.params.keyword, req.params.page_no || 1, req.params.api_key)); //Default value for page_no is 1
}

//Function to get Tumblr images
exports.TumblrImages= function(req, res){

    //Check if scroll
    if (req.params.scroll == 'scroll')
    {
        //Call service function to scrap Tumble images with scroll
        TumblrScrapperService.ScrollImages(req.params.keyword, res);
    }
    else {
        //Call service function to scrap Tumblr images
        res.json(TumblrScrapperService.ScrapImages(req.params.keyword));
    }
    
}

//Function to get TikTok images
exports.TikTokImages = function(req, res){

    //Call service function to scrap TikTok images
    res.json(TikTokScrapperService.ScrapImages(req.params.keyword));
}


//Function to get Weibo Images
exports.WeiboImages = function(req, res) {

    //Call service function to scrap Weibo images
    res.send(WeiboScrapperService.ScrapImages(req.params.keyword, req.params.page_no || 1)); //Default value for page_no is 1
}

//Function to get We Heart It Images
exports.WeHeartItImages = function(req, res) {

    //Call service function to scrap WeHeartIt images
    res.json(WeHeartItScrapperService.ScrapImages(req.params.keyword, req.params.page_no || 1)); //Default value for page_no is 1
}


//Function to get Youtube videos 
exports.YoutubeVideos = function(req, res) {

    //Call service function to scrap Youtube videos
    return res.json(YoutubeScrapperService.ScrapVideos(req.params.keyword, req.params.page_index));
}


//Function to get Metacafe videos
exports.MetacafeVideos = function(req, res){

    //Call service function to scrap Metacafe videos
    return res.json(MetacafeScrapperService.ScrapVideos(req.params.keyword));
}