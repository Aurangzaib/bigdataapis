module.exports = function(app) {

    //Include social media scrapper controller
    var SocialMediaScrapper = require('../controllers/socialmediascrapperController');

    //Define images routes
    app.route('/twitterimages/:keyword')
        .get(SocialMediaScrapper.TwitterImages);

    app.route('/instagramimages/:keyword/:end_cursor?')
        .get(SocialMediaScrapper.InstagramImages);

    app.route('/pinterestimages/:keyword/:bookmark?')
        .get(SocialMediaScrapper.PinterestImages);

    app.route('/flickrimages/:keyword/:page_no?/:api_key?')
        .get(SocialMediaScrapper.FlickrImages);

    app.route('/tumblrimages/:keyword/:scroll?')
        .get(SocialMediaScrapper.TumblrImages);

    app.route('/tiktokimages/:keyboard')
        .get(SocialMediaScrapper.TikTokImages);

    app.route('/weiboimages/:keyword/:page_no?')
        .get(SocialMediaScrapper.WeiboImages);

    app.route('/weheartitimages/:keyword/:page_no?')
       .get(SocialMediaScrapper.WeHeartItImages);

    
    //Define videos routes
    app.route('/youtubevideos/:keyword/:page_index?')
       .get(SocialMediaScrapper.YoutubeVideos);

    app.route('/metacafevideos/:keyword')
       .get(SocialMediaScrapper.MetacafeVideos);

    /*app.route('/rumblevideos/:keyword')
       .get(SocialMediaScrapper.RumbleVideos);

    app.route('/flickrvideos/:keyword/:page_no?/:api_key?')
       .get(SocialMediaScrapper.FlickrVidoes);

    app.route('/tumblrvideos/:keyword/:scroll')
       .get(SocialMediaScrapper.TumblrVideos);*/
}